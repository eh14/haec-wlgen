# Haec-Cubie Workload Generator #


## Quick Start ##

Install by cloning the repo. You also need to clone and install. Only the twisted-utils is in the cheeseshop.

 * https://bitbucket.org/eh14/twisted-utils
 * https://bitbucket.org/eh14/django-viewutils

```
# install django viewutils
cd /tmp
git clone https://bitbucket.org/eh14/django-viewutils
cd django-viewutils
sudo python setup.py install

# install twisted-utils
sudo pip install twisted-utils
```


System dependencies:

* python-twisted
* fabric

Python dependencies (install via pip):

* Django
* pythonioc
* txsockjs

Run it with fabric using

```
    # deploy only necessary first time.
    fab deploy
    # Stop with Ctrl-C
    fab run:url=http://127.0.0.1:8980
```