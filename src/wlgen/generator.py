'''

@author: eh14
'''
import twutils
import pythonioc
from twisted.internet import defer, threads
from twisted.python import threadpool, log
import requests
from wlgen import statistics
import json
import os
import time
import random
import math


# ##
# Maps trigger-IDs to their meanings in the videoplatform. Those values have to be kept in sync with the VP
# otherwise it will not work as expected.
# ##
TRIGGER_PEAK = 10
TRIGGER_LOGIN = 20
TRIGGER_LOGOUT = 21
TRIGGER_UPLOAD = 30


#
# general timeout for requests to produce a response.
# Otherwise the process might hang forever.
REQUEST_TIMEOUT = 25


class SettingsSelector(object):
    def __init__(self, args):
        self._rand = random.Random(time.time())
        self._profileIds = args.profileIds
        self._anyProfile = args.anyProfileName
        self._transcodingProbability = args.transcodingProb
        
        self._qualityMode = args.qualityMode
        
    def getProfile(self):
        if self._rand.random() < self._transcodingProbability:
            return self._rand.choice(self._profileIds)
        else:
            return self._anyProfile
             
             
    def getQualityMode(self):
        return self._qualityMode
    
    
class BaseGenerator(object):
    
    _reactor = pythonioc.Inject('reactor')
    _stats = pythonioc.Inject(statistics.StatKeeper)
    
    def __init__(self, args):
        
        self._config = args
        self._threadPool = threadpool.ThreadPool(maxthreads=100, name='Worker Threads')
        self._nextCall = None
        self._shuttingDown = False
        self._start = None
        
        self._settings = SettingsSelector(args)
        self._rand = random.Random(time.time())
        
    @property
    def requestsPerHour(self):
        return self._config.requestsPerHour
    
    @property
    def loadShape(self):
        return self._config.loadShape
    
    @property
    def loadPeriod(self):
        return self._config.loadPeriodLength
    
    @property
    def loadVariance(self):
        return self._config.loadVariance
    
    def start(self):
        
        self._start = time.time()
        
        self._threadPool.start()
        
        self._run()
        
    def stopThreadpool(self):
        self._threadPool.stop()
        
    @defer.inlineCallbacks
    def shutdown(self):
        
        self._shuttingDown = True
        
        while self._stats.numRunningRequests() > 0:
            log.msg('Waiting for pending requests (%d) to terminate.' % self._stats.numRunningRequests())
            yield twutils.deferredSleep(self._reactor, 1)
            
        self.stopThreadpool()
        
        try:
            yield self.postActions()
        except Exception as e:
            print "Error performing postaction", e
        
        if self._config.resultFile:
            self._writeStats(self._config.resultFile)
            
        try:
            self._reactor.stop()
        except Exception as e:
            print "Error stopping the reactor. Maybe already stopped"
            print e
        
    @defer.inlineCallbacks
    def _prepareRecord(self, record):
        record.setSettings(self._settings.getProfile(), self._settings.getQualityMode())
        record.start()
        response = yield threads.deferToThreadPool(self._reactor, self._threadPool, self._requestRandomVideo)
        record.doneGetName(response.text)
        
        if response.status_code != 302:
            record.abort("Server did not select random video", response)
        
    @defer.inlineCallbacks
    def _performPriorityRequest(self, record):
        record.setPriorityUser()
        yield self._performTrigger(TRIGGER_LOGIN)
        try:
            yield twutils.deferredSleep(self._reactor, self._config.priorityRequestDelay)
            # prepare it after sleep, otherwise the times are getting messed up.
            yield self._prepareRecord(record)
            yield self._performRequest(record)
        finally:
            yield self._performTrigger(TRIGGER_LOGOUT)
    
    @defer.inlineCallbacks
    def _performRequest(self, record):
        
        # first, try to get a link to request the stream URL.
        # The response may be a 200 containing a task ID which means a transcoding had been started
        response = yield self._runInThread(self._requestVideo, record)
        if response.status_code == 200:
            record.setRequestType('transcode')
            response = yield self._waitForTranscoder(response.text, record)
        elif response.status_code == 302:
            record.setRequestType('download')
        
        # after all, we'll get a 302 
        if response.status_code != 302:
            record.abort("Error trying to get request URL", response)
        
        # finally, get the URL for streaming
        yield self._getStreamUrl(record, response.headers['location'])
        
        # and stream it...
        response = yield self._runInThread(self._streamVideo, record)
        
        assert not record.isAborted(), "record aborted but we're still alive. What's wrong?"
        
        record.done()
        
    @defer.inlineCallbacks
    def _waitForTranscoder(self, taskId, record):
        # after this timestamp, we abort waiting
        lastWait = time.time() + self._config.maxTransWaitTime
        
        #
        # make the URL. Basically just a taskstatus poller-URL. Result
        # should be a json string
        url = '%s/%s?%s' % (self._config.targetUrl,
                          self._config.transPollUrl,
                          self._makeUrlArgs(taskId=taskId))
        
        while time.time() < lastWait:
            response, taskDetails = yield self._waitAndGetTaskStatus(url, record)
            # if waiting, keep waiting
            if taskDetails['status'] == 'waiting':
                continue
            
            # if running or successful already, end loop
            if taskDetails['status'] in ['running', 'success']:
                break
            # otherwise (failure, cancelled), just abort the record.
            else:
                record.abort('Transcoding did not start.', response)
                
        # If the while-loop has NOT been left early (due to break), we assume it's timed out.
        else:
            record.abort('Transcoding wait timeout. Transcoding did not start within %d seconds' 
                         % self._config.maxTransWaitTime)
            
        
        #
        # Poll while its running.
        lastRun = time.time() + self._config.maxTransRunTime
        
        #
        # manage timeout
        while time.time() < lastRun:
            response, taskDetails = yield self._waitAndGetTaskStatus(url, record)
            
            # waiting-status should have been managed in the last loop
            if taskDetails['status'] == 'waiting':
                record.abort('Invalid task transition', response)
                
            # still running, try next time.
            if taskDetails['status'] == 'running':
                continue
            
            # successful, end loop
            if taskDetails['status'] == 'success':
                break
            
            # otherwise, just fail the record.
            else:
                record.abort('Transcoding failed.', response)
                
        # If the while-loop has NOT been left early (due to break), we assume it's timed out.
        else:
            record.abort('Transcoding running timeout. Transcoding did not finish within %d seconds' 
                            % self._config.maxTransWaitTime)
            
        assert taskDetails['status'] == 'success'
        
        # request the video again and return that response
        vidResponse = yield self._runInThread(self._requestVideo, record)
        defer.returnValue(vidResponse) 
        
    @defer.inlineCallbacks
    def _waitAndGetTaskStatus(self, url, record):
        yield twutils.deferredSleep(self._reactor, 10)
        response = yield self._runInThread(requests.get, url, timeout=REQUEST_TIMEOUT)
        if response.status_code != 200:
            record.abort('Error checking transcoding status', response)
        
        taskDetails = json.loads(response.text)
            
        defer.returnValue((response, taskDetails))
    
    def _requestRandomVideo(self):
        return requests.get(self._config.targetUrl + '/' + self._config.randomVideoUrl, allow_redirects=False, timeout=REQUEST_TIMEOUT)
    
    def _requestVideo(self, record):
        # make an url of
        # baseURL
        # streamURL-suffix
        # name-parameter
        # video-settings-parameters (joined with &)
        url = self._config.targetUrl + '/' + self._config.requestVideoUrl
        url += '?name={name}&profile={profile}&qualityMode={qm}&priority={priority}'.format(name=record.name,
                                                                                            profile=record.profile,
                                                                                            qm=record.qualityMode,
                                                                                            priority=record.priorityUser) 
        return requests.get(url, allow_redirects=False, timeout=REQUEST_TIMEOUT)
    
    
    @defer.inlineCallbacks
    def _getStreamUrl(self, record, videoUrl):
        
        realUrl = videoUrl + '&' + self._makeUrlArgs(details=1, download=1)
        for _i in range(120):
            # request the stream URL
            response = yield self._runInThread(requests.get, realUrl, allow_redirects=False, timeout=REQUEST_TIMEOUT)
            
            # if the slave is booting, let's sleep for one second
            if response.status_code == 102:
                yield twutils.deferredSleep(self._reactor, 1.0)
            else:
                # I got a redirect, that should be the location where I should download the video
                if response.status_code == 302:
                    record.doneRequestUrl(response.headers['location'], json.loads(response.text))
                    break
                
                # I got something else... Error
                else:
                    record.abort("Expected redirect or 102", response)
                
        # Loop was not left, I never got the download URL
        else:
            record.abort('Timeout waiting to get a request URL')
            
    def _streamVideo(self, record):
        response = requests.get(record.requestUrl, allow_redirects=False, stream=True, timeout=REQUEST_TIMEOUT)
        if not response.ok:
            record.abort('Unexpected server result', response)
        
        # download the file to /dev/null
        with open(os.devnull, 'w') as out:
            for block in response.iter_content(1024):
                if not block:
                    break
                out. write(block)
                
        return response
    
    @defer.inlineCallbacks
    def _performTrigger(self, triggerId):
        """
        Performs a trigger call.
        """
        self._stats.logTrigger(triggerId)
        url = '%s/%s?triggerId=%s' % (self._config.targetUrl,
                          self._config.triggerUrl,
                          triggerId)
        yield threads.deferToThreadPool(self._reactor, self._threadPool, requests.post, url, timeout=REQUEST_TIMEOUT)
        
        
    def _writeStats(self, outFile):
        if self._stats.getRunningRequests() > 0:
            log.msg("WARNING: There are still running requests but we're trying to write statistics.")
        
        with open(outFile, 'w') as out:
            print >> out, " ".join(["start",
                                    "name",
                                    "profile",
                                    "quality",
                                    'priority',
                                    "server",
                                    "type",
                                    "latency",
                                    "download",
                                    "streamusability",
                                    "response",
                                    "message"])
            
            for record in self._stats.getDoneRecords():
                self._printRecord(out, record)
                            
    def _printRecord(self, out, r):
        if not isinstance(r, statistics.RequestRecord):
            return

        print >> out, r.startTime, \
            r.name, \
            r.profile, \
            r.qualityMode, \
            r.priorityUser, \
            r.streamServer, \
            r.type, \
            r.latency, \
            r.downloadTime, \
            r.streamUsability, \
            r.errorResponse.status_code if r.errorResponse else '""', \
            '"%s (%s)"' % (r.errorMessage, r.errorResponse.text.splitlines()[0] if r.errorResponse else '')
            
    def postActions(self):
        """
        Hook for subclasses to perform actions
        after the generator run but before reactor
        shutdown.
        """
        pass
    
    def _run(self):
        """
        Abstract method for subclasses.
        Runs the generator.
        """
        raise NotImplementedError()
    
    def _runInThread(self, function, *args, **kwargs):
        """
        Helper function running a function in the local thread pool.
        """
        return threads.deferToThreadPool(self._reactor, self._threadPool, function, *args, **kwargs)

    def _makeUrlArgs(self, **params):
        """
        Creates an url, appending all params to the url string appropriately.
        """
        
        return '&'.join('%s=%s' % (k, v) for k, v in params.iteritems())
        
class ContinuousGenerator(BaseGenerator):
    
    # length of one standard sinus period
    SIN_PERIOD = math.pi * 2.0
    
    def __init__(self, *args, **kwargs):
        BaseGenerator.__init__(self, *args, **kwargs)
        self._periodStart = 0
        self._shutdownScheduled = False
        self._triggerStarted = False
        
    @property
    def requestsPerHour(self):
        """
        Override from base class returning the current requests per hour
        which may change due to non-flat load shapes.
        """
        return self._getCurrentRpm()
        
    def setRequestsPerHour(self, requestsPerHour):
        self._config.requestsPerHour = int(requestsPerHour)
        self._run() 
        
    def setLoadShape(self, loadShape):
        if loadShape not in ['flat', 'sine']:
            raise AttributeError('invalid load shape')
        self._config.loadShape = loadShape
        self._run()
        
    def setLoadVariance(self, loadVariance):
        if not (0 <= loadVariance <= 1):
            raise AttributeError('Load-Variance must be within 0..1')
        
        self._config.loadVariance = loadVariance
        self._run()
        
    def setLoadPeriod(self, loadPeriod):
        if loadPeriod <= 0:
            raise AttributeError('Load Period must be greater 0')
        
        self._config.loadPeriodLength = loadPeriod
        self._run()
        
    def _getCurrentRpm(self):
        if self._config.loadShape == 'flat':
            return self._config.requestsPerHour
        elif self._config.loadShape == 'sine':
            timePassed = (time.time() - self._periodStart)
            while timePassed > self._config.loadPeriodLength:
                timePassed -= self._config.loadPeriodLength
            
            periodPos = (timePassed / self._config.loadPeriodLength)
            sinX = periodPos * self.SIN_PERIOD
            multiplier = math.sin(sinX)
            
            newRpm = self._config.requestsPerHour + self._config.requestsPerHour * (multiplier * self._config.loadVariance)
            # print "periodpos:", periodPos, "sine pos", sinX, "rpm", newRpm
            return max(1, newRpm)
            
        else:
            raise RuntimeError('Invalid load shape ' + self._config.loadShape)
        
    def _run(self):
        self._runTriggers()
        self._scheduleShutdown()
        
        self._periodStart = time.time()
        # call any pending calls, otherwise
        # nothing will change since that call would schedule the next.
        if self._nextCall:
            self._nextCall.cancel()
            

        if self._config.requestsPerHour == 0:
            return
        
        log.msg('SCHEDULING shape: "%s" rpm: %.2f, period: %.2f, variance: %.2f' % (self._config.loadShape,
                                                                                 self._config.requestsPerHour,
                                                                                 self._config.loadPeriodLength,
                                                                                 self._config.loadVariance))
        
        @defer.inlineCallbacks
        def runAndSchedule():
            
            if not self._shuttingDown:
                self._nextCall = self._reactor.callLater(3600.0 / self._getCurrentRpm(), runAndSchedule)
            
            try:
                record = self._stats.newRecord()
                
                # check whether we should do a priority-request
                if self._rand.random() >= (1 - self._config.priorityRequestProb):
                    yield self._performPriorityRequest(record)
                else:
                    yield self._prepareRecord(record)
                    yield self._performRequest(record)
                
            except statistics.RequestAborted as e:
                log.msg('Request aborted. ' + str(e))
            except Exception as e:
                print "<<<<<", e , ">>>>>"
                record.abort('Internal error!')
            
        self._nextCall = self._reactor.callLater(3600.0 / self._getCurrentRpm(), runAndSchedule)
        
    def _scheduleShutdown(self):
        """
        If max runtime is set, schedule the runtime already
        """
        
        if self._shutdownScheduled:
            return
        
        if not (self._config.runtime > 0):
            return
        
        self._shutdownScheduled = True
        
        self._reactor.callLater(self._config.runtime, self.shutdown)
        
    def _runTriggers(self):
        """
        Starts the trigger. But starts them only once.
        """
        if self._triggerStarted:
            return
        
        self._triggerStarted = True
        
        def getJitteredInterval(interval):
            jitter = interval * ((self._rand.random() * self._config.triggerJitter * 2) - self._config.triggerJitter)
            assert -self._config.triggerJitter * interval <= jitter <= self._config.triggerJitter * interval
            return interval + jitter
        
        def scheduleTrigger(interval, function):
            self._reactor.callLater(getJitteredInterval(interval), function)
        
        def triggerPeak():
            scheduleTrigger(self._config.triggerPeak, triggerPeak)
            self._performTrigger(TRIGGER_PEAK)
            
        def triggerUpload():
            scheduleTrigger(self._config.triggerUpload, triggerUpload)
            self._performTrigger(TRIGGER_UPLOAD)
            
        if self._config.triggerPeak > 0:
            scheduleTrigger(self._config.triggerPeak, triggerPeak)
            
        if self._config.triggerUpload > 0:
            scheduleTrigger(self._config.triggerUpload, triggerUpload)
            
class ReplayingGenerator(BaseGenerator):
    def _run(self):
        
        latest = 0
        with open(self._config.replaySchedule, 'r') as schedule:
            for line in schedule:
                line = line.strip()
                # ignore empty lines and comments
                if not len(line) or line[0] == '#':
                    continue
                
                split = line.split(' ')
                
                if split[0] == 'TriggerRecord':
                    delay = float(split[1])
                    self._reactor.callLater(float(split[1]), self._performTrigger, split[2])
                elif split[0] == 'RequestRecord':
                    delay, name, profile, qualityMode, priorityUser = split[1:]
                    delay = float(delay)
                    self._reactor.callLater(delay,
                                            self._runRequest,
                                            name,
                                            profile,
                                            qualityMode,
                                            priorityUser.lower() in ['yes', 'true', '1'])
                else:
                    raise AttributeError('invalid record found')
                
                # update latest
                if delay > latest:
                    latest = delay
        
        # schedule a shutdown 1 second after the last scheduled replay-record
        self._reactor.callLater(float(latest + 1), self._finishReplay)
        
    def _finishReplay(self):
        log.msg('Replay finished. Shutting down the reactor...')
        self.shutdown()
        
        
    @defer.inlineCallbacks
    def _runTrigger(self, triggerId):
        pass
    
    
    @defer.inlineCallbacks
    def _runRequest(self, name, profile, qualityMode, priorityUser):
        assert priorityUser in [False, True]
        record = self._stats.newRecord()
        record.start()
        record.setSettings(profile, qualityMode)
        record.doneGetName(name)
        
        log.msg('Replaying request for %s (%s, %s)' % (name, profile, qualityMode))
        
        try:
            if priorityUser:
                yield self._performPriorityRequest(record)
            else:
                yield self._performRequest(record)
                
        except statistics.RequestAborted as e:
            log.msg('Request aborted. ' + str(e))
        except Exception as e:
            print "<<<<<", e , ">>>>>"
            record.abort('Internal error!')
        

class RecordingGenerator(ContinuousGenerator):
    
    def postActions(self):
        if self._stats.getRunningRequests() > 0:
            log.msg("WARNING: There are still running requests but we're trying to write the schedule")
        with open(self._config.recordSchedule, 'w') as sched:
            for rec in self._stats.getDoneRecords():
                delay = rec.replayTime() - self._start
                assert delay > 0
                rec.printIntoLog(sched, delay)
                
    @defer.inlineCallbacks
    def _prepareRecord(self, record):
        yield BaseGenerator._prepareRecord(self, record)
        
        record.abort("Recording schedule only")
        
