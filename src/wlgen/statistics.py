'''

@author: eh14
'''
import time
import re
import pythonioc

class RequestAborted(Exception):
    """
    Helper Exception to leave execution flow when a request has been aborted.
    """

class StatKeeper(object):
    
    _recordId = 1
    
    statPusher = pythonioc.Inject('statPushFactory')
    
    
    def __init__(self):
        self._runningRecords = set()
        self._doneRecords = []
        
        self._metaStatistics = []
        
    def clean(self):
        timeLimit = time.time() - 60
        self._doneRecords = [r for r in self._doneRecords if r.doneTime > timeLimit]
        
    def newRecord(self):
        def doneCallback(record):
            self._runningRecords.remove(record)
            self._doneRecords.append(record)
            self.statPusher.recordDone(record)
            
        r = RequestRecord(self._recordId, doneCallback, doneCallback)
        self._recordId += 1
        self._runningRecords.add(r)
        return r
    
    def logTrigger(self, triggerId):
        self._doneRecords.append(TriggerRecord(triggerId))
    
    def getDoneRecords(self):
        return self._doneRecords 
    
    def getRunningRequests(self):
        return len(self._runningRecords)

    def numRunningRequests(self):
        return len(self._runningRecords)
    
class Record(object):
    def printIntoLog(self, out, delay):
        print >> out, "%s %.2f %s" % (type(self).__name__, delay, ' '.join(map(str, self.getLogDetails())))
    
    def getLogDetails(self):
        """returns details (a list) to log the record for replay"""
        return []
    
    def replayTime(self):
        """
        Returns the time when the record should be replayed.
        """
        raise NotImplementedError("overwrite in subclasses")
    
class TriggerRecord(Record):
    def __init__(self, triggerId):
        self._triggerId = triggerId
        self._start = time.time()
        
        print "Triggered ID ", triggerId
    @property
    def triggerId(self):
        return self._triggerId
    @property
    def start(self):
        return self._start
    
    def getLogDetails(self):
        return [self.triggerId]
    
    @property
    def doneTime(self):
        return self._start
    
    def replayTime(self):
        return self._start
    
class RequestRecord(Record):
    """
    Records the lifecycle of a request. It is implemented as a state machine
    """
    PHASE_PRISTINE = 0
    PHASE_STARTED = 1
    PHASE_NAMED = 2
    PHASE_URL_REQUESTED = 3
    PHASE_DONE = 4
    
    # extracts the server information form a
    # URL
    patternExtractServer = re.compile('^http://([^\?/]+).*$')
    
    def __init__(self, id_, doneCallback, failedCallback):
        self._id = id_
        self._doneCallback = doneCallback
        self._failedCallback = failedCallback
        
        
        self._phase = self.PHASE_PRISTINE

        #
        # Request type
        #  * download: simple download of file, maybe waiting for bootup
        #  * transcode: triggered transcoding
        #  * error: what caused error
        # 'download', 'transcode', 'error'
        self._type = None
        
        # set when in phase-started
        self._startTime = None
        
        self._name = None
        self._nameTime = None
        
        #
        # marks the request to be sent as priority user.
        self._priorityUser = False
        
        # url on the slave where to download video
        self._requestUrl = None
        # the time, when the video request URL was obtained
        self._requestUrlTime = None
        
        # video details
        self._videoDetails = None
        
        # time when everything was done (or an error occured)
        self._doneTime = None
        
        #
        # Some error details
        self._errorMessage = 'ok'
        self._errorResponse = None
        
        self._profile = None
        self._qualityMode = None
        
        
        #
        # statistical information, calculated after 
        # the RequestRecord is finished
        self._latency = 0
        self._downloadTime = 0
        self._streamUsability = 0
        self._streamServer = 'unknown'
    
    def getLogDetails(self):
        return [self.name, self.profile, self.qualityMode, self.priorityUser]
    
    @property
    def type(self):
        return self._type
    @property
    def id(self):
        return self._id
    
    @property
    def latency(self):        
        return self._latency
    
    @property
    def downloadTime(self):
        return self._downloadTime
    
    @property
    def streamUsability(self):
        return self._streamUsability
    
    @property
    def startTime(self):
        return self._startTime
    
    @property
    def videoDetails(self):
        return self._videoDetails
    
    def start(self):
        self._startTime = time.time()
        self._advanceToPhase(self.PHASE_STARTED)
        
    def replayTime(self):
        return self._nameTime
        
    @property
    def name(self):
        return self._name
    
    @property
    def requestUrl(self):
        return self._requestUrl
    
    @property
    def doneTime(self):
        return self._doneTime
    
    @property
    def nameTime(self):
        return self._nameTime
    
    @property
    def profile(self):
        return self._profile
    
    @property
    def qualityMode(self):
        return self._qualityMode
    
    @property
    def priorityUser(self):
        return self._priorityUser
    
    @property
    def streamServer(self):
        return self._streamServer
    
    @property
    def errorMessage(self):
        return self._errorMessage
    
    @property
    def errorResponse(self):
        return self._errorResponse
        
    def doneGetName(self, name):
        self._name = name
        self._nameTime = time.time()
        self._advanceToPhase(self.PHASE_NAMED)
        
    def doneRequestUrl(self, requestUrl, videoDetails):
        self._requestUrl = requestUrl
        self._requestUrlTime = time.time()
        self._videoDetails = videoDetails
        self._advanceToPhase(self.PHASE_URL_REQUESTED)
        
    def setRequestType(self, rType):
        assert rType in ['download', 'transcode', 'error'], 'invalid request type'
        self._type = rType
        
    def setPriorityUser(self):
        """
        Marks the record as being sent from a priority user. Default is false, so
        this method does not have to be called.
        """
        self._priorityUser = True
    def setSettings(self, profile, qualityMode):
        self._profile = profile
        self._qualityMode = qualityMode
        
    def done(self):
        self._doneTime = time.time()
        self._advanceToPhase(self.PHASE_DONE)
        
        # prefinal step: calculate statistics
        self._calcStats()
        
        # FINAL step: call the callback so the stat store knows what to do
        self._doneCallback(self)
        print """RequestRecord Done:
            name: {video}
            type: {type}
            latency: {latency}
            priority: {priority}
            downloadTime: {dltime}
            usability: {usability}
            server: {server}
        """.format(video=self._name,
                   type=self._type,
                   latency=self._latency,
                   dltime=self._downloadTime,
                   priority=self._priorityUser,
                   usability=self._streamUsability,
                   server=self._streamServer
                   )
        
        
    def _calcStats(self):
        assert self._phase == self.PHASE_DONE, "stats must be calculated after finishing"
        if self.isAborted():
            self._latency = self._doneTime - self._startTime
            self._downloadTime = 0
        else:
            self._latency = self._requestUrlTime - self._nameTime
            self._downloadTime = self._doneTime - self._requestUrlTime
        
        if self._type == 'error':
            self._streamUsability = -1
        else:
            self._streamUsability = float(self._videoDetails['duration']) / float(self.downloadTime)
            assert self._requestUrl, "No request url set although the request seemed to be OK"
            m = self.patternExtractServer.match(self._requestUrl)
            assert m, "Invalid request URL %s" % self._requestUrl
            self._streamServer = m.groups(1)
        
    def abort(self, reason, errorResponse=None):
        """
        Marks the record as aborted.
        This method raises a RequestAborted exception in order to disrupt execution
        flow.
        """
        
        self._errorMessage = reason
        self._errorResponse = errorResponse
        self._doneTime = time.time()
        self._advanceToPhase(self.PHASE_DONE)
        
        oldType = self._type        
        self.setRequestType('error')
        
        # prefinal step: calculate statistics
        self._calcStats()
        
        print """RequestRecord Aborted:
            reason: {reason}
            name: {video}
            type: {type}
            response: {response}
            priority: {priority}
            oldtype (before error): {oldtype}
            latency: {latency}
            downloadTime: {dltime}
            usability: {usability}
            server: {server}
        """.format(video=self._name,
                   reason=reason,
                   type=self._type,
                   oldtype=oldType,
                   response=errorResponse.status_code if errorResponse else 'unknown response',
                   latency=self._latency,
                   priority=self._priorityUser,
                   dltime=self._downloadTime,
                   usability=self._streamUsability,
                   server=self._streamServer
                   )
        
        # FINAL step: call the callback so the stat store knows what to do
        self._failedCallback(self)
        if errorResponse is not None:
            extraInfo = '(%s [%s])' % (errorResponse.text, errorResponse.status_code)
        else:
            extraInfo = ""
        raise RequestAborted("%s %s" % (reason, extraInfo))
    
    def isAborted(self):        
        return self._type == 'error'
    
    def _advanceToPhase(self, newPhase):
        assert newPhase > self._phase, "Invalid phase transition (%s -> %s)" % (self._phase, newPhase)
        self._phase = newPhase
        
