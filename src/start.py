'''

@author: eh14
'''
import argparse
from wlgen import generator, statistics
from twisted.internet import reactor, protocol, task
from twisted.python import log
from twisted.manhole.telnet import ShellFactory
import pythonioc
import sys
import frontend
import twutils
from txsockjs.factory import SockJSFactory
from twisted.web.server import Site
import json
import time
import collections

def _handleArgs():

    parser = argparse.ArgumentParser()
    
    # urls
    parser.add_argument("--target-url", dest='targetUrl', default='http://localhost:8980')
    parser.add_argument("--random-video-url", dest="randomVideoUrl", default='exp/randomVideo')
    parser.add_argument("--request-video-url", dest="requestVideoUrl", default='video/transcoding')
    parser.add_argument("--transcoding-poll-url", dest="transPollUrl", default="tasks/details")
    parser.add_argument("--webserver-port", dest='webserverPort', help="Server port of the generator where clients can poll results, or change values", default=10310, type=int)
    parser.add_argument("--websocket-port", dest='websocketPort', help="Server port of the websocket providing realtime results", default=10311, type=int)
    
    # generator statistic settings
    parser.add_argument("--load-shape", dest="loadShape",
                        help="Load shape to perform.", default='flat', type=str, choices=['flat', 'sine'])
    parser.add_argument("--requests-per-hour", dest='requestsPerHour',
                        help="requests to send per hour. This will be medium value for sinus shapes", default=360, type=float)
    parser.add_argument("--load-period", dest="loadPeriodLength",
                        help="Length of one period when using sinus-shaped load. Ignored otherwise. In seconds", default=10, type=float)
    parser.add_argument("--load-variance", dest="loadVariance",
                        help="Relative variance for sinus-shaped load. Ignored otherwise. Ranges 0..1", default=0.5, type=float)
    parser.add_argument("--transcoding-probability", dest="transcodingProb",
                        help="Probability of using a transcoding profile. One of the profiles will be chosen at random.", default=0, type=float)
    
    # for recording/replay
    parser.add_argument("--runtime", dest='runtime',
                        help='Set to terminate after n seconds', default=0, type=float)
    parser.add_argument("--record-schedule", dest='recordSchedule',
                        help="Create workload schedule. Argument is the schedule file.", default=None)
    parser.add_argument("--replay-schedule", dest='replaySchedule',
                        help="Replay a workload schedule. Argument is the schedule file.", default=None)
    
    # request settings
    parser.add_argument("--any-profile-name", dest='anyProfileName',
                        help="name of the pseudo-profile when the profile does not matter.", default='anyprofile')
    parser.add_argument("--profile-ids", dest="profileIds",
                        help="List of profile ids (comma separated).", type=lambda v: v.split(','), default="universalsmartphonelow,universalsmartphonemedium,universalsmartphonehighdef,legacy3gp,flash")
    parser.add_argument("--quality-mode", dest="qualityMode",
                        help="Quality mode to use for all the requests.", default="normal")
    
    # timing settings
    parser.add_argument('--max-trans-wait-time', dest="maxTransWaitTime",
                        help="max number of seconds, we will wait before the transcoding starts", default=120)
    parser.add_argument('--max-trans-run-time', dest="maxTransRunTime",
                        help="max number of seconds, we will wait for a transcoding to finish", default=600)
    
    # output statistics
    parser.add_argument("--result-file", help="File to write results. Use when doing replay or limited run.", dest="resultFile", default=None)
    
    def interval01Type(value):
        value = float(value)
        if not (0 <= value <= 1):
            raise argparse.ArgumentTypeError('Value must be between [0..1]')
        return value
    
    # situation triggering
    parser.add_argument("--trigger-jitter", dest='triggerJitter',
                        help="Jitter [0-1] to be applied between the trigger requests", type=interval01Type, default=0.01)
    parser.add_argument("--peak-trigger-interval", dest='triggerPeak',
                        help="Number of seconds between a peak is triggered. 0 is off.", type=float, default=0)
    parser.add_argument("--upload-trigger-interval", dest='triggerUpload',
                        help="Number of seconds between an uploaded video is triggered. 0 is off.", type=interval01Type, default=0)
    parser.add_argument("--priority-request-prob", dest='priorityRequestProb',
                        help="Probability of a user login->priority-request->logout-sequence.", type=float, default=0)
    parser.add_argument("--priority-delay", dest="priorityRequestDelay",
                        help="Number of seconds between a priority login and the the first priority request", type=float, default=50)
    parser.add_argument("--trigger-url", dest='triggerUrl',
                        help="URL-suffix used to trigger some events. The url will be called using the trigger-ID as parameter (?triggerId=x)", type=str, default="exp/trigger")
    return parser.parse_args()


class StatPushProtocol(protocol.Protocol):
    def connectionMade(self):
        log.msg('Connection made')        
        self.factory.connections.add(self)
        
    def connectionLost(self, reason):
        log.msg('Connection lost %s' % str(reason))
        self.factory.connections.remove(self)
        
class StatPushFactory(protocol.Factory):
    protocol = StatPushProtocol
    
    connections = set()
    
    _stats = pythonioc.Inject(statistics.StatKeeper)
    _gen = pythonioc.Inject('generator')
    
    _lastDoneRecords = []
    _lastAvgLatencies = collections.deque(maxlen=5)
    
    def broadcastStats(self,):
        recs = self._lastDoneRecords
        
        self._lastDoneRecords = []
        
        if len(recs):
            self._lastAvgLatencies.append(sum([r.latency for r in recs]) / len(recs))
        else:
            self._lastAvgLatencies.append(0)
        
        msg = {'time':time.time(),
               'doneRecords':0,
               'runningRequests':self._stats.numRunningRequests(),
               'avgLatency': sum(self._lastAvgLatencies) / len(self._lastAvgLatencies),
               'rph':self._gen.requestsPerHour,
               'downloads':0,
               'transcodes':0,
               'errors':0}
        
        if len(recs) > 0:
            msg['doneRecords'] = len(recs)
            msg['downloads'] = len([r for r in recs if r.type == 'download'])
            msg['transcodes'] = len([r for r in recs if r.type == 'transcode'])
            msg['errors'] = len([r for r in recs if r.type == 'error'])
        
        rawMsg = json.dumps(msg)
        for conn in self.connections:
            conn.transport.write(rawMsg)
    
    def recordDone(self, record):
        # ignore trigger-records
        if isinstance(record, statistics.TriggerRecord):
            return
        
        self._lastDoneRecords.append(record)
        
statKeeper = pythonioc.Inject(statistics.StatKeeper)
    
if __name__ == '__main__':
    args = _handleArgs()
    pythonioc.registerServiceInstance(reactor, 'reactor')
    pythonioc.registerService(statistics.StatKeeper)
    
    # clean up old statistics only if
    #    runtime is not limited
    #    wlgen is not replaying
    #    wlgen is not recording
    if args.runtime == 0 and not args.replaySchedule and not args.recordSchedule:
        cleanJob = twutils.createLoopingCall(reactor, statKeeper.clean)
        cleanJob.start(3, False)
    
    if args.recordSchedule:
        log.msg('Starting the recorder')
        gen = generator.RecordingGenerator(args)
    elif args.replaySchedule:
        log.msg('Starting the replayer')
        gen = generator.ReplayingGenerator(args)
    else:
        log.msg('Starting the contiuous generator')
        gen = generator.ContinuousGenerator(args)
        
    log.startLogging(sys.stdout)
    gen.start()
    
    pythonioc.registerServiceInstance(args, 'config')
    pythonioc.registerServiceInstance(gen, 'generator')
    
    serverThreadPool, serverInstance = frontend.createServer(reactor)
    
    reactor.listenTCP(args.webserverPort,  # @UndefinedVariable
                      Site(serverInstance))
    
    statPushFactory = StatPushFactory()
    pythonioc.registerServiceInstance(statPushFactory, 'statPushFactory')
    
    pushLooper = twutils.createLoopingCall(reactor, statPushFactory.broadcastStats)
    pushLooper.start(1, False)
    
    manholePort = reactor.listenTCP(0, ShellFactory())  # @UndefinedVariable
    log.msg('Started manhole on port %s' % str(manholePort.getHost().port))
    
    reactor.listenTCP(args.websocketPort, SockJSFactory(statPushFactory))  # @UndefinedVariable
    log.msg('Listening on port %s, websocket on port %s' % (args.webserverPort, args.websocketPort))
    
    reactor.addSystemEventTrigger('before', 'shutdown', gen.shutdown)  # @UndefinedVariable
    reactor.addSystemEventTrigger('before', 'shutdown', serverThreadPool.stop)  # @UndefinedVariable
    
    
    reactor.run()  # @UndefinedVariable
    
