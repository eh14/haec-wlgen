#!/usr/bin/env python
"""
Thread that uses a django frontend application
"""

import os
import sys
from twisted.python import threadpool

from twisted.web.wsgi import WSGIResource

def createServer(reactor):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'frontend.settings')
        
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    tpool = threadpool.ThreadPool()
    tpool.start()
    return (tpool, WSGIResource(reactor, tpool, application))