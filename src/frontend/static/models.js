$(function() {
    var models = {};

    models.RecordValue = Backbone.Model.extend({
        allowToEdit : function(account) {
            return false;
        }
    });

    model.RecordValues = Backbone.Collections.extend({
        model : models.SlaveActivityValue,
        url : $.getServerUrl('getRecordStats'),
    });

    _.defaults($, {
        wlgen : {}
    });

    _.defaults($.wlgen, {
        models : models
    });
});