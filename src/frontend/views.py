'''

@author: eh14
'''
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.views.decorators.http import require_GET, require_POST
from django.template.context import Context
from django.template.loader import get_template
from wlgen import statistics
import pythonioc
import djangoviewutils
from twisted.internet import threads
from django.views.decorators.csrf import csrf_exempt


statKeeper = pythonioc.Inject(statistics.StatKeeper)
reactor = pythonioc.Inject('reactor')
gen = pythonioc.Inject('generator')

# inject the global config (actually the command line arguments)
config = pythonioc.Inject('config')

@require_GET
def home(request):
    ctx = Context({'initialValues':_getStats()})
    template = get_template('home.html')
    return HttpResponse(template.render(ctx))


@djangoviewutils.response_CORS_all
@djangoviewutils.json_response
@require_GET
def getStats(request):
    return _getStats()

def _getStats():
    records = threads.blockingCallFromThread(reactor, statKeeper.getDoneRecords)
    
    data = [{'id':record.id,
      'starttime':record.startTime * 1000,
      'latency':record.latency,
      'downloadTime' : record.downloadTime,
      'type':record.type,
      'videoName':str(record.videoDetails['name']) if record.videoDetails else '',
      'streamUsability':record.streamUsability} for record in records]
        
    return data

@djangoviewutils.response_CORS_all
@djangoviewutils.json_response
@require_GET
def getConfig(request):
    return {'websocketport':config.websocketPort}


@djangoviewutils.response_CORS_all
@djangoviewutils.json_response
@require_GET
def getSettings(request):
    return {
            'requestsPerHour':gen.requestsPerHour,
            'loadPeriod':gen.loadPeriod,
            'loadVariance':gen.loadVariance,
            'loadShape':gen.loadShape,
            'concurrentRequests':threads.blockingCallFromThread(reactor, statKeeper.getRunningRequests),
           }

@djangoviewutils.response_CORS_all
@require_POST
@csrf_exempt
@djangoviewutils.get_params(reqPerHour=djangoviewutils.intConverter)
def setRequestsPerHour(request, reqPerHour):
    assert 1 <= reqPerHour <= 48000, "requests per hour out of range"
    try:
        gen.setRequestsPerHour(reqPerHour)
        return HttpResponse('ok')
    except AttributeError as e:
        HttpResponseBadRequest('The workload generator does not seem to be runtime-adjustable (%s)' % e)

@djangoviewutils.response_CORS_all
@require_POST
@csrf_exempt
@djangoviewutils.get_params('loadShape')
def setLoadShape(request, loadShape):
    try:
        gen.setLoadShape(loadShape)
        return HttpResponse('ok')
    except AttributeError as e:
        HttpResponseBadRequest('The workload generator does not seem to be runtime-adjustable (%s)' % e)

@djangoviewutils.response_CORS_all
@require_POST
@csrf_exempt
@djangoviewutils.get_params(loadVariance=float)
def setLoadVariance(request, loadVariance):
    try:
        gen.setLoadVariance(loadVariance)
        return HttpResponse('ok')
    except AttributeError as e:
        HttpResponseBadRequest('The workload generator does not seem to be runtime-adjustable (%s)' % e)

@djangoviewutils.response_CORS_all
@require_POST
@csrf_exempt
@djangoviewutils.get_params(loadPeriod=float)
def setLoadPeriod(request, loadPeriod):
    try:
        gen.setLoadPeriod(loadPeriod)
        return HttpResponse('ok')
    except AttributeError as e:
        HttpResponseBadRequest('The workload generator does not seem to be runtime-adjustable (%s)' % e)
