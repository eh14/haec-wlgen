from django.conf.urls import patterns, include, url

from django.contrib import admin
from frontend import settings
import os
from django.conf.urls.static import static
admin.autodiscover()
import views

settings.TEMPLATE_DIRS.append(os.path.join(os.path.abspath(__file__), 'templates')) 

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'frontend.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^home/?$', views.home),
    url(r'^home/recordstats?$', views.getStats),
    url(r'^settings/?$', views.getSettings),
    url(r'^requestsPerHour/?$', views.setRequestsPerHour),
    url(r'^loadShape/?$', views.setLoadShape),
    url(r'^loadVariance/?$', views.setLoadVariance),
    url(r'^loadPeriod/?$', views.setLoadPeriod),
    url(r'^config/?$', views.getConfig),
    
)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)