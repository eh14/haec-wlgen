from fabric.decorators import task
from fabric.context_managers import cd, lcd
from fabric.operations import local

@task
def deploy():
    with lcd('src'):
        local('python manage.py collectstatic  --noinput')
@task
def run(speed=720, url='http://localhost:9990', trans=0, prio=0):
    with lcd('src'):
        local('python start.py --target-url=%s --requests-per-hour=%s --transcoding-probability=%s --priority-request-prob=%s' % 
                            (url, speed, trans, prio))
        

@task
def record(speed=3600 * 2, url='http://localhost:9990', record='schedule',
           runtime=10, trans=0):
    """
    Records a schedule to file {record}.
    
    @param speed: requests per hour
    @param url: base url
    @param record: output file
    @param runtime: number of seconds
    @param trans: probability of transcoding requests (0-1)
    """
    with lcd('src'):
        local('python start.py ' + 
              '--target-url=%s ' % url + 
              '--requests-per-hour=%s ' % speed + 
              '--runtime=%s ' % runtime + 
              '--record-schedule=%s ' % record + 
              '--transcoding-probability=%s' % trans)


@task
def replay(schedule, results, url='http://localhost:9990'):
    """
    Replays a recorded file.
    
    @param replay: file to replay
    @param url: base url to videoplatform
    """
    
    with lcd('src'):
        local('python start.py ' + 
              '--target-url=%s ' % url + 
              '--replay-schedule=%s ' % schedule + 
              '--result-file=%s ' % results)
        
